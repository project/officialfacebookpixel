CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * FAQ
 * Maintainers


INTRODUCTION
------------

This plugin will install a Facebook Pixel on your Drupal page. The Official Facebook Pixel allows
you to fire PageView events when people visit your website. Tracking pixel events can help you
understand the actions people are taking on your website. You can then use this information to
make adjustments accordingly in your advertising campaigns.


REQUIREMENTS
------------
 * Drupal 8.x
 * PHP 5.6 or greater

INSTALLATION
------------

 1. Download the zip file attached to the current release.
 2. Go to your Drupal 8.x site, sign in and enter the "Extend" tab.
 3. Click "+ Install new module" and choose the zip file.
 4. Enable the "Official Facebook Pixel" module by check the checkbox and click "Install".
 5. Go to the "Configuration" tab and click the "Official Facebook Pixel".
 6. Paste your Facebook pixel ID into the text input box and click "Save configuration".

DEPRECATED
----------

This module is deprecated in favour of communities module: https://www.drupal.org/project/facebook_pixel



