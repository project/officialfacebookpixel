# Contributing to Official Facebook Pixel
While this module was not maintained anymore by Facebook anymore, the name might be misleading meanwhile.
Maintenance is taken over by the community.

Please contribute according to the [Drupal contribution Guide](https://www.drupal.org/community/contributor-guide/) and
following the [Drupal Coding Standards](https://www.drupal.org/docs/develop/standards)

## Maintainer
### Drupal 8+9
[mmbk](https://www.drupal.org/u/mmbk)

### Drupal 7
As I don't maintain any Drupal 7 sites anymore, I'm not able to
maintain the Drupal 7 Version. If you need change to the D7 version,
feel free to apply as a maintainer.
